import os
import io
import csv
import pandas as pd
from serve import predict_unseen_data, get_model_api, align_data
from flask import Flask, render_template, request, jsonify, make_response, send_file

app = Flask(__name__)

@app.route("/")
def home():
    return "customers feedback analysis"

@app.route("/api")
def api():
    predict_unseen_data()
    # return show()
    return "Prediction successful"

@app.route( '/about' )
def about():
    return render_template('about.html')

@app.route('/upload')
def form():
    return """
        <html>
            <body>
                <h1>Upload a CSV file to predict the data</h1>

                <form action="/upload_file" method="post" enctype="multipart/form-data">
                    <input type="file" name="input_file" />
                    <input type="submit" />
                </form>
            </body>
        </html>
    """

@app.route('/upload_file', methods=['POST'])
def upload():

    # Input file
    file = request.files['input_file']
    if not file:
        return "No file"

    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    filename_path = os.path.join(THIS_FOLDER, file.filename)

    # Put input file in dataframe
    # sheet = pd.read_csv(filename_path, encoding='cp1252', sep='\t', header=None)
    return filename_path

def get_csv():
    csv_path = "./predicted_results_1526300218/predictions_all.csv"
    # csv_path = file_path
    csv_file = open(csv_path, 'r')
    csv_obj = csv.DictReader(csv_file)
    csv_list = list(csv_obj)
    return csv_list

@app.route( "/show" )
def show():
    template = 'show.html'
    object_list = get_csv()
    # return jsonify( object_list=object_list )
    return render_template(template, object_list=object_list)

@app.route( "/show_test", methods = ['GET', 'POST'])
def show_test():
    # input_data = request.json
    # app.logger.info("api_input: " + str(input_data))
    # # output_data = get_model_api(input_data)
    # output_data=align_data({"input": "some input", "output": input_data})
    # app.logger.info("api_output: " + str(output_data))
    # response = jsonify(output_data)
    return jsonify({"results": "success"})


# @app.route('/<name>')
# def hello_name(name):
#     return "Hello {}!".format(name)

# HTTP Errors handlers
@app.errorhandler(404)
def url_error(e):
    return """
    Wrong URL!
    <pre>{}</pre>""".format(e), 404

@app.errorhandler(500)
def server_error(e):
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == "__main__":
    app.run(host='127.0.0.1', debug=True)
